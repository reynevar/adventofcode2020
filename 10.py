#!/usr/bin/python3

def readData(filename):
    with open(filename) as o:
        data = o.readlines()
        data = [int(x) for x in data]
    data.sort()
    return data

def calcJolts(data):
    currJolts = 0
    jolt1Diff = 0
    jolt3Diff = 1
    for val in data:
        if val in range(currJolts, currJolts+4):
            if val - currJolts == 3:
                jolt3Diff += 1
            elif val - currJolts == 1:
                jolt1Diff += 1
            currJolts = val
    
    return jolt1Diff*jolt3Diff

dat = readData("input10")

res = calcJolts(dat)
print(res)

dat.append(dat[-1]+3)

paths = {0: 1}
for r in dat:
  paths[r] = paths.get(r-3,0) + paths.get(r-2,0) + paths.get(r-1,0)

print(paths[dat[-1]])
