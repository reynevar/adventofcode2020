with open('input1') as f:
    numbersStr = f.readlines()
numbers = [int(i) for i in numbersStr]
for n in numbers:
    if (2020-n) in numbers:
        print((2020-n) *n)
        break
for n in numbers:
    for m in numbers:
        if (2020-n-m) in numbers:
            print((2020-n-m) * n * m)
            break
    else:
        continue
    break
