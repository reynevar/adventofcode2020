class Player:

    def __init__(self):
        self.currentPos = [0, 0]
        self.patternLen = 10 # to be updated
        self.moveVec = [1,1]

    def setPatternLen(self, value):
        self.patternLen = value

    def setMoveVector(self, x, y):
        self.moveVec = [x, y]

    def updatePosition(self):
        tmpPos = [self.currentPos[0] + self.moveVec[0], self.currentPos[1] + self.moveVec[1]]
        self.currentPos= [ tmpPos[0] % self.patternLen, tmpPos[1]]

    def printPos(self):
        print(str(self.currentPos[0]) + " : " + str(self.currentPos[1]))

    def resetPos(self):
        self.currentPos = [0,0]

def countTrees(p, data, vec):
    p.resetPos()
    p.setMoveVector(vec[0], vec[1])
    treeNum = 0
    while(p.currentPos[1]+ p.moveVec[1] < len(data)):
        p.updatePosition()
        s = data[p.currentPos[1]]
        if s[p.currentPos[0]] == '#':
            treeNum += 1
    return treeNum


def main():
    with open('input3') as f:
        data = f.readlines()

    p = Player()
    p.setPatternLen(len(data[0])-1)
    res = countTrees(p, data, [3,1])
    print(res)
    res *= countTrees(p, data, [1,1])
    res *= countTrees(p, data, [5,1])
    res *= countTrees(p, data, [7,1])
    res *= countTrees(p, data, [1,2])
    print(res)

if __name__ == "__main__":
    # execute only if run as a script
    main()
