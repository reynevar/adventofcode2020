with open('input6') as f:
    data = f.read().split('\n\n')
    
data1 = [x.replace('\n','') for x in data]
res1 = sum(len(set(i)) for i in data1)
print(res1)

res2 = 0
for i in data:
    i = i.split('\n')
    resList = [set(x) for x in i if x]
    resList = resList[0].intersection(*resList)
    res2 += len(resList)

print(res2)
