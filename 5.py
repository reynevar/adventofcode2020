def calcSeatId(bp):
    row = bp[:7]
    row = row.replace('F', '0')
    row = row.replace('B', '1')
    rowInt = int('0b' + row, 2)

    col = bp[-4:]
    col = col.replace('L', '0')
    col = col.replace('R' , '1')
    colInt = int('0b' + col, 2)
    # print('row: ' + str(rowInt) + ' col: ' + str(colInt)) # debug
    return rowInt * 8 + colInt

with open("input5") as f:
    data = f.readlines()

bpList = [calcSeatId(x) for x in data]
bpList.sort()

print('top seat: ' + str(bpList[-1]))

for i in range(0,len(bpList)-1):
    if bpList[i+1] - bpList[i] == 2:
        print('my seatID: ' + str(bpList[i]+1))
        break
