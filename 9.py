#!/usr/bin/python3

def checkValue(val, lst):
    nlst = [x for x in lst if x < val]
    for n in nlst:
        for n2 in nlst:
            if n+n2 == val:
                return True

    return False

def getSumList(val, lst):
    for enum,x in enumerate(lst):
        if x < val:
            s = 0
            sumlist = []
            for x2 in lst[enum:]:
                s += x2
                if s <= val:
                    sumlist.append(x2)
                    if s == val and len(sumlist) > 1:
                        return sumlist
                elif s > val:
                    break
    return list()

data = []
with open("input9") as o:
    data = o.readlines()
    data = [int(x) for x in data]

preamble = 25

currentCheck = preamble

for val in data[preamble:]:
    if not checkValue(val, data[currentCheck-preamble:currentCheck]):
        print("found error: " + str(val))
        res = getSumList(val, data)
        if len(res) > 2:
            res.sort()
            print("found sum: " + str(res[0] + res[-1]))
        exit(0)
    currentCheck += 1