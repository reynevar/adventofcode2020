#!/usr/bin/python3

import copy

def changeNext(instructions, lastPos):
    res = instructions[:]
    retpos = -1
    for pos,val in enumerate(res):
        if pos > lastPos:
            if val[0] == 'jmp':
                res[pos][0] = 'nop'
                return res, pos
            elif val[0] == 'nop':
                res[pos][0] = 'jmp'
                return res, pos
    return res, retpos

def countAccum(instructions):
    currentInstruct = 0
    accum = 0
    exitNormally = False
    while True:
        if len(instructions) == currentInstruct:
            exitNormally = True
            break
        inst,val,wasHere = instructions[currentInstruct]
        if wasHere == True:
            break
        else:
            instructions[currentInstruct][2] = True
        if val[0] == '-':
            val = (-1)*int(val[1:])
        else:
            val = int(val[1:])
        if inst == 'acc':
            accum += val
            currentInstruct += 1
        elif inst == 'jmp':
            currentInstruct += val
        elif inst == 'nop':
            currentInstruct += 1

    return accum, exitNormally

with open("input8") as f:
    data = f.readlines()

instructions = []
for i in data:
    instructions.append(i.split())
    instructions[-1].append(False)

accum, exitNormally = countAccum(copy.deepcopy(instructions))
print('first result: ' + str(accum))

lastChangedPos = -1
while True:
    tmpInstrusctions, lastChangedPos = changeNext(copy.deepcopy(instructions), lastChangedPos)

    if lastChangedPos == -1:
        print("Cheched all possibilities, solution not found")
        break

    accum, exitNormally = countAccum(tmpInstrusctions)
    if exitNormally:
        print("Proper exit with acc: " + str(accum))
        exit(0)