#!/usr/bin/python3
from copy import deepcopy

EMPTY = 'L'
TAKEN = '#'

def readData(filename):
    res = []
    rows = 0
    cols = 0
    with open(filename) as o:
        data = o.readline()
        while data:
            res.append(list(data.strip()))
            data = o.readline()
    rows = len(res)
    cols = len(res[0])
    return res,rows,cols

def findOccupiedSeats(data, x, y):
    neighbors = []
    rows, cols = len(data[0]), len(data)
    if x > 0:
        neighbors.append(data[x-1][y])
        if y > 0:
            neighbors.append(data[x-1][y-1])
        if y < rows - 1:
            neighbors.append(data[x-1][y+1])
    if x < cols-1:
        neighbors.append(data[x+1][y])
        if y > 0:
            neighbors.append(data[x+1][y-1])
        if y < rows - 1:
            neighbors.append(data[x+1][y+1])
    if y > 0:
        neighbors.append(data[x][y-1])
    if y < rows - 1:
        neighbors.append(data[x][y+1])
    return sum(1 for x in neighbors if x == '#')

def findVisibleSeats(data,x,y):
    count = 0
    dirs = [(-1, -1), (-1, 0), (-1, 1),
            (0,  -1),          (0,  1),
            (1,  -1), (1,  0), (1, 1)]
    for (dx, dy) in dirs:
        nx, ny = (x+dx, y+dy)
        while ((0 <= nx <= len(data)-1) and
               (0 <= ny <= len(data[0])-1)):
            if data[nx][ny] == EMPTY:
                break
            if data[nx][ny] == TAKEN:
                count += 1
                break
            nx, ny = (nx+dx, ny+dy)
    return count

def applyRules(data, rows, cols, isPartA = True):
    rounds = 0
    wasChanged = True
    sumret = 0

    while wasChanged:
        currData = deepcopy(data)
        wasChanged = False
        rounds += 1

        for row in range(rows):
            for col in range(cols):
                val = data[row][col]
                adjCount = findOccupiedSeats(currData,row,col) if isPartA else findVisibleSeats(currData,row,col)
                if val == EMPTY and adjCount == 0:
                    data[row][col] = TAKEN
                    wasChanged = True
                elif val == TAKEN and (isPartA and adjCount >= 4 or (not isPartA and adjCount >=5)):
                    data[row][col] = EMPTY
                    wasChanged = True

    for r in range(len(data)):
        for c in range(len(data[r])):
            if data[r][c] == TAKEN:
                sumret += 1

    return sumret

dat,rows,cols = readData('input11')
newData = deepcopy(dat)
print(applyRules(dat,rows,cols))
print(applyRules(newData,rows,cols, False))
