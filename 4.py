import re

def checkPass(passwd):
    requiredKeys = ["ecl","pid","eyr","hcl","byr","iyr", "hgt"]
    eyeColors = ['amb','blu','brn','gry','grn','hzl','oth']
    hairPattern = re.compile("^#([0-9]|[a-f]){6}$")
    pidPattern = re.compile("^([0-9]){9}$")
    if not all(item in dic.keys() for item in requiredKeys):
        return False

    if int(passwd['byr']) in range(1920,2003) and\
            int(passwd['iyr']) in range(2010,2021) and\
            int(passwd['eyr']) in range(2020,2031) and\
            ((passwd['hgt'][-2:] == 'cm' and int(passwd['hgt'][:-2]) in range(150,194)) or\
            (passwd['hgt'][-2:] == 'in' and int(passwd['hgt'][:-2]) in range(59,77))) and\
            re.search(hairPattern, passwd['hcl']) and\
            passwd['ecl'] in eyeColors and\
            re.search(pidPattern, passwd['pid']):
        return True
    return False


with open("input4") as f:
    data = f.read()
    validCount = 0
    parsed = data.split('\n\n')
    passwds = []
    for it in parsed:
        v = re.split(' |\n|:', it)
        dic = ({key : value for (key, value) in [(k, v) for k,v in zip(v[:-1:2], v[1::2])]})
        if checkPass(dic):
            validCount += 1
        passwds.append(dic)
    print(validCount)

