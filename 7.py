import re

class Node:
    def __init__(self, name, value = 1):
        self.children = []
        self.name = name
        self.count = value
    
    def add_child(self, name, value):
        new_child = Node(name, value)
        self.children.append(new_child)

    def is_leaf(self):
        return not self.children

    def __str__(self):
        if self.is_leaf():
            return str(self.name) + ":" + str(self.count)
        return '{name}:{value} [{children}]'.format(name=self.name, value = self.count, children=', '.join(map(str, self.children)))
    
    def getName(self):
        return self.name

    def get_children(self):
        return self.children

    def get_count(self):
        return self.count

search_string = "shiny gold"

nodes = []
with open('input7') as f:
    data = f.readlines()
data2 = dict()
childBags = dict()
for line in data:
    if 'contain no other bags' in line:
        continue

    line_after = re.sub(' bag[s]?[\.]?|\n', '', line)
    line_after = re.split(', | contain ', line_after)
    parent = line_after[0]
    n = Node(name=parent)
    for i in line_after[1:]:
        val = i[2:]
        value = int(i[0])
        n.add_child(val, value)
        if val not in data2.keys():
            data2[val] = [parent]
        else:
            data2[val].append(parent)
    nodes.append(n)

parents = data2[search_string]
allParents = parents
while len(parents) != 0:
    newParents = []
    for p in parents:
        if p in data2.keys():
            newParents.extend(data2[p])
    parents = newParents
    allParents.extend(newParents)

print(len(set(allParents)))

def getCount(val, arr): 
    cnt = val.get_count()
    for i in val.get_children():
        item = next((x for x in arr if x.getName() == i.getName()), None)
        if (item != None):
            cnt += i.get_count() * getCount(item, arr)
        else:
            cnt += i.get_count()
    return cnt

item = next((x for x in nodes if x.getName() == search_string), None)
res2 = getCount(item, nodes) -1
print(res2)