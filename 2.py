import re

def isPassCorrect(line):
    lo, hi, ch, pwd = re.split('-| |: ', line)
    if pwd.count(ch) in range(int(lo),int(hi)+1):
        return True
    else:
        return False


def isPassCorrect2(line):
    p1, p2, ch, pwd = re.split('-| |: ', line)
    arr = [pwd[int(p1)-1], pwd[int(p2)-1]]
    if ch in arr and arr[0] != arr[-1]:
        return True
    else:
        return False

with open('input2') as f:
    data = f.readlines()
print(len(data))
v = sum(True for x in data if isPassCorrect(x))
print(v)

v2 = sum(True for x in data if isPassCorrect2(x))
print(v2)
